Pour installer le projet:

1/Créer une base de donnée Postgres avec les valeurs suivantes

port:5432
nom: postgres
username: postgres
password: postgres

2/Y executer le script contenus dans le dossier "SQL Scripts"

3/Lancer l'application spring avec l'IDE de votre choix

Schéma de Base de données

![alt text](/ReadMePics/Bdd_Schema.png)

Diagramme UML des classes Java

![alt text](/ReadMePics/uml_diagram.png)
