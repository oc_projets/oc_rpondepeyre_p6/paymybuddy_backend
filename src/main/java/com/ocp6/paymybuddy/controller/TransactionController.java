package com.ocp6.paymybuddy.controller;

import java.math.BigDecimal;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ocp6.paymybuddy.config.exceptions.AccountNotFoundException;
import com.ocp6.paymybuddy.config.exceptions.NoMoneyException;
import com.ocp6.paymybuddy.config.jwt.JwtUtils;
import com.ocp6.paymybuddy.dto.CurrentUserDto;
import com.ocp6.paymybuddy.dto.TransactionDto;
import com.ocp6.paymybuddy.services.TransactionService;

@RestController
@RequestMapping("/transfer")
public class TransactionController {

    @Autowired
    JwtUtils utils;

    @Autowired
    TransactionService service;

    @Transactional
    @PostMapping("")
    public ResponseEntity<CurrentUserDto> transferMoney(@RequestHeader("Authorization") String authHeader,
            @RequestBody TransactionDto body) throws AccountNotFoundException, NoMoneyException {
        CurrentUserDto result = service.transferMoney(
                utils.getUsernamefromToken(utils.getTokenFromHeader(authHeader)),
                body);
        return ResponseEntity.status(200).body(result);

    }

    @GetMapping("commission")
    public ResponseEntity<BigDecimal> getCommisson() {
        return ResponseEntity.status(200).body(TransactionService.COMMISSION_PERCENTAGE);
    }

}
