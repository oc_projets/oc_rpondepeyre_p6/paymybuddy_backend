package com.ocp6.paymybuddy.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ocp6.paymybuddy.config.exceptions.DatabaseErrorException;
import com.ocp6.paymybuddy.config.jwt.JwtUtils;
import com.ocp6.paymybuddy.dto.AuthRequest;
import com.ocp6.paymybuddy.dto.Subscriber;
import com.ocp6.paymybuddy.dto.TokenVerify;
import com.ocp6.paymybuddy.services.AuthenticationService;

@RestController
@RequestMapping("/auth")
public class AuthController {

    @Autowired
    private AuthenticationService service;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    private JwtUtils utils;

    @PostMapping("/authent")
    public ResponseEntity<String> login(@RequestBody AuthRequest request) throws BadCredentialsException {
        try {

            final UserDetails user = service.loadUserByUsername(request.getUsername());

            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword()));

            return ResponseEntity.status(200).body(utils.generateToken(user));

        } catch (UsernameNotFoundException e) {
            return ResponseEntity.status(401).body("No user found with this e-mail");

        } catch (BadCredentialsException e) {
            return ResponseEntity.status(401).body("Incorrect Password");

        } catch (Exception e) {
            return ResponseEntity.status(401).body("Unknown Error");
        }

    }

    @GetMapping("/exist")
    public Boolean verifyMail(@RequestParam String mail) {
        return service.verifyMail(mail);
    }

    @PostMapping("/subscribe")
    public ResponseEntity<String> subscribe(@RequestBody Subscriber user) throws DatabaseErrorException {
        service.subscribe(user, encoder);
        return new ResponseEntity<>("Account successfully created", HttpStatus.CREATED);
    }

    @PostMapping("/token")
    public ResponseEntity<Boolean> verifyToken(@RequestBody TokenVerify token) {
        return new ResponseEntity<>(utils.validateToken(token.getToken(), service.loadUserByUsername(token.getMail())),
                HttpStatus.OK);
    }

}
