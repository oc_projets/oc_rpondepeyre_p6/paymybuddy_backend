package com.ocp6.paymybuddy.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ocp6.paymybuddy.config.exceptions.AccountNotFoundException;
import com.ocp6.paymybuddy.config.exceptions.BadRequestException;
import com.ocp6.paymybuddy.config.jwt.JwtUtils;
import com.ocp6.paymybuddy.dto.BanqueDto;
import com.ocp6.paymybuddy.dto.CurrentUserDto;
import com.ocp6.paymybuddy.dto.TransferDto;
import com.ocp6.paymybuddy.dto.UserDto;
import com.ocp6.paymybuddy.services.UserService;

@RestController
@RequestMapping("/user")
public class UserController {

        @Autowired
        JwtUtils utils;

        @Autowired
        UserService service;

        @GetMapping("")
        public ResponseEntity<CurrentUserDto> getCurrentUserInfos(@RequestHeader("Authorization") String authHeader)
                        throws AccountNotFoundException {

                CurrentUserDto user = service
                                .findCurrentUserByMail(
                                                utils.getUsernamefromToken(utils.getTokenFromHeader(authHeader)));

                return ResponseEntity.status(200).body(user);

        }

        @GetMapping("/find")
        public ResponseEntity<UserDto> getUserByMail(@RequestParam String mail) throws AccountNotFoundException {
                UserDto user = service.findUserByMail(mail);
                return ResponseEntity.status(200).body(user);
        }

        @GetMapping("/addfriend")
        public ResponseEntity<CurrentUserDto> addConnection(@RequestHeader("Authorization") String authHeader,
                        @RequestParam String mail)
                        throws BadRequestException, AccountNotFoundException {
                CurrentUserDto currentUser = service.addConnection(
                                utils.getUsernamefromToken(utils.getTokenFromHeader(authHeader)),
                                mail);
                return ResponseEntity.status(200).body(currentUser);
        }

        @GetMapping("/removefriend")
        public ResponseEntity<CurrentUserDto> removeConnection(@RequestHeader("Authorization") String authHeader,
                        @RequestParam String mail)
                        throws BadRequestException, AccountNotFoundException {
                CurrentUserDto currentUser = service.removeConnection(
                                utils.getUsernamefromToken(utils.getTokenFromHeader(authHeader)),
                                mail);
                return ResponseEntity.status(200).body(currentUser);
        }

        @PostMapping("/addbank")
        public ResponseEntity<CurrentUserDto> addBankAccount(@RequestHeader("Authorization") String authHeader,
                        @RequestBody BanqueDto bank) throws AccountNotFoundException {
                CurrentUserDto result = service.addBankAccount(
                                utils.getUsernamefromToken(utils.getTokenFromHeader(authHeader)),
                                bank);
                return ResponseEntity.status(200).body(result);

        }

        @GetMapping("/removebank")
        public ResponseEntity<CurrentUserDto> removeBankAccount(@RequestHeader("Authorization") String authHeader,
                        @RequestParam int bankid)
                        throws BadRequestException, AccountNotFoundException {
                CurrentUserDto result = service.removeBankAccount(
                                utils.getUsernamefromToken(utils.getTokenFromHeader(authHeader)),
                                bankid);
                return ResponseEntity.status(200).body(result);
        }

        @PostMapping("/deposit")
        public ResponseEntity<CurrentUserDto> deposit(@RequestHeader("Authorization") String authHeader,
                        @RequestBody TransferDto transfer) throws AccountNotFoundException {
                CurrentUserDto result = service
                                .deposit(utils.getUsernamefromToken(utils.getTokenFromHeader(authHeader)), transfer);
                return ResponseEntity.status(200).body(result);

        }

        @PostMapping("/withdraw")
        public ResponseEntity<CurrentUserDto> withdraw(@RequestHeader("Authorization") String authHeader,
                        @RequestBody TransferDto transfer) throws Exception {
                CurrentUserDto result = service
                                .withdraw(utils.getUsernamefromToken(utils.getTokenFromHeader(authHeader)), transfer);
                return ResponseEntity.status(200).body(result);

        }
}
