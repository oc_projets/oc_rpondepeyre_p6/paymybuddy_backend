package com.ocp6.paymybuddy.services;

import java.math.BigDecimal;
import java.util.Optional;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ocp6.paymybuddy.config.exceptions.AccountNotFoundException;
import com.ocp6.paymybuddy.config.exceptions.BadRequestException;
import com.ocp6.paymybuddy.config.exceptions.NoMoneyException;
import com.ocp6.paymybuddy.dto.BanqueDto;
import com.ocp6.paymybuddy.dto.CurrentUserDto;
import com.ocp6.paymybuddy.dto.TransferDto;
import com.ocp6.paymybuddy.dto.UserDto;
import com.ocp6.paymybuddy.mappers.BanqueMapper;
import com.ocp6.paymybuddy.mappers.CurrentUserMapper;
import com.ocp6.paymybuddy.mappers.UserMapper;
import com.ocp6.paymybuddy.model.Banque;
import com.ocp6.paymybuddy.model.UserModel;
import com.ocp6.paymybuddy.repository.UserRepository;

@Service
public class UserService {

    final String accountMessage = "The requested account could not be found";

    @Autowired
    private UserRepository repository;

    public CurrentUserDto findCurrentUserByMail(String mail) throws AccountNotFoundException {
        Optional<UserModel> userBox = repository.findByMail(mail);
        if (userBox.isPresent()) {
            return CurrentUserMapper.instance.UsertoCurrentUserDto(userBox.get());
        } else {
            throw new AccountNotFoundException(accountMessage);
        }
    }

    public UserDto findUserByMail(String mail) throws AccountNotFoundException {
        Optional<UserModel> userBox = repository.findByMail(mail);
        if (userBox.isPresent()) {
            return UserMapper.instance.UsertoUserDto(userBox.get());
        } else {
            throw new AccountNotFoundException(accountMessage);
        }
    }

    public CurrentUserDto addConnection(String currentUserMail, String addUserMail)
            throws BadRequestException, AccountNotFoundException {

        if (!currentUserMail.equalsIgnoreCase(addUserMail)) {
            Optional<UserModel> currentUserOpt = repository.findByMail(currentUserMail);
            Optional<UserModel> addUserOpt = repository.findByMail(addUserMail);
            if (currentUserOpt.isPresent()) {
                if (addUserOpt.isPresent()) {
                    UserModel currentUser = currentUserOpt.get();
                    UserModel addUser = addUserOpt.get();
                    if (!relationExists(currentUser, addUser)) {
                        Set<UserModel> relations = currentUser.getRelations();
                        relations.add(addUser);
                        currentUser.setRelations(relations);
                        UserModel finaluser = repository.save(currentUser);
                        return CurrentUserMapper.instance.UsertoCurrentUserDto(finaluser);
                    } else {
                        throw new BadRequestException("This User is already in your connections list");
                    }
                } else {
                    throw new BadRequestException("User not found with this mail");
                }
            } else {
                throw new AccountNotFoundException(accountMessage);
            }
        } else {
            throw new BadRequestException("You cannot add yourself as friend");
        }

    }

    public CurrentUserDto removeConnection(String currentUserMail, String removeUserMail)
            throws AccountNotFoundException {
        Optional<UserModel> currentUserOpt = repository.findByMail(currentUserMail);
        Optional<UserModel> addUserOpt = repository.findByMail(removeUserMail);
        if (currentUserOpt.isPresent() && addUserOpt.isPresent()) {
            UserModel currentUser = currentUserOpt.get();
            UserModel addUser = addUserOpt.get();
            currentUser.getRelations().remove(addUser);
            UserModel finaluser = repository.save(currentUser);
            return CurrentUserMapper.instance.UsertoCurrentUserDto(finaluser);

        } else {
            throw new AccountNotFoundException(accountMessage);
        }
    }

    public boolean relationExists(UserModel currentUser, UserModel addUser) {
        return currentUser.getRelations()
                .stream()
                .anyMatch(relation -> relation.getMail().equals(addUser.getMail()));
    }

    public CurrentUserDto addBankAccount(String currentUserMail, BanqueDto addBank) throws AccountNotFoundException {
        Optional<UserModel> currentUserOpt = repository.findByMail(currentUserMail);
        if (currentUserOpt.isPresent()) {
            UserModel currentUser = currentUserOpt.get();
            Banque banque = BanqueMapper.instance.DtotoModel(addBank);
            banque.setUtilisateur(currentUser);
            Set<Banque> userbanks = currentUser.getBanques();
            userbanks.add(banque);
            currentUser.setBanques(userbanks);
            UserModel finaluser = repository.save(currentUser);
            return CurrentUserMapper.instance.UsertoCurrentUserDto(finaluser);
        } else {
            throw new AccountNotFoundException(accountMessage);
        }
    }

    public CurrentUserDto removeBankAccount(String currentUserMail, int bankid)
            throws AccountNotFoundException {
        Optional<UserModel> currentUserOpt = repository.findByMail(currentUserMail);
        if (currentUserOpt.isPresent()) {
            UserModel currentUser = currentUserOpt.get();
            Set<Banque> banks = currentUser.getBanques();
            Banque banktoRemove = banks.stream().filter(element -> (element.getId() == bankid)).findFirst()
                    .orElseThrow(
                            () -> new AccountNotFoundException("No bank account founded with specified informations"));
            banks.remove(banktoRemove);
            currentUser.setBanques(banks);
            UserModel finaluser = repository.save(currentUser);
            return CurrentUserMapper.instance.UsertoCurrentUserDto(finaluser);

        } else {
            throw new AccountNotFoundException(accountMessage);

        }
    }

    @Transactional
    public CurrentUserDto deposit(String currentUserMail, TransferDto transfer) throws AccountNotFoundException {
        Optional<UserModel> currentUserOpt = repository.findByMail(currentUserMail);
        if (currentUserOpt.isPresent()) {
            UserModel currentUser = currentUserOpt.get();
            Set<Banque> banks = currentUser.getBanques();
            Banque banktoPerform = banks.stream().filter(element -> (element.getId() == transfer.getBank().getId()))
                    .findFirst()
                    .orElseThrow(
                            () -> new AccountNotFoundException("No bank account founded with specified informations"));

            if (banktoPerform.getIban().equals(transfer.getBank().getIban())
                    && banktoPerform.getBic().equals(transfer.getBank().getBic())) {
                // CALL BANK SERVICE HERE
                BigDecimal currentWallet = currentUser.getWallet();
                BigDecimal newWallet = currentWallet.add(transfer.getAmount());
                currentUser.setWallet(newWallet);
                UserModel finaluser = repository.save(currentUser);
                return CurrentUserMapper.instance.UsertoCurrentUserDto(finaluser);

            } else {
                throw new AccountNotFoundException("No bank account founded with specified informations");
            }

        } else {
            throw new AccountNotFoundException(accountMessage);

        }
    }

    @Transactional
    public CurrentUserDto withdraw(String currentUserMail, TransferDto transfer)
            throws AccountNotFoundException, NoMoneyException {
        Optional<UserModel> currentUserOpt = repository.findByMail(currentUserMail);
        if (currentUserOpt.isPresent()) {
            UserModel currentUser = currentUserOpt.get();
            Set<Banque> banks = currentUser.getBanques();
            Banque banktoPerform = banks.stream().filter(element -> (element.getId() == transfer.getBank().getId()))
                    .findFirst()
                    .orElseThrow(
                            () -> new AccountNotFoundException("No bank account founded with specified informations"));

            if (banktoPerform.getIban().equals(transfer.getBank().getIban())
                    && banktoPerform.getBic().equals(transfer.getBank().getBic())) {
                // CALL BANK SERVICE HERE
                BigDecimal currentWallet = currentUser.getWallet();
                BigDecimal newWallet = currentWallet.subtract(transfer.getAmount());
                if (newWallet.compareTo(BigDecimal.valueOf(0)) >= 0) {
                    currentUser.setWallet(newWallet);
                    UserModel finaluser = repository.save(currentUser);
                    return CurrentUserMapper.instance.UsertoCurrentUserDto(finaluser);
                } else {
                    throw new NoMoneyException("You don't have enough money on your account to perform this action");
                }

            } else {
                throw new AccountNotFoundException("No bank account founded with specified informations");
            }

        } else {
            throw new AccountNotFoundException(accountMessage);

        }
    }

}
