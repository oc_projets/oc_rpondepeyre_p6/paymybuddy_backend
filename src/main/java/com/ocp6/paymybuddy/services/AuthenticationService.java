package com.ocp6.paymybuddy.services;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.ocp6.paymybuddy.config.exceptions.DatabaseErrorException;
import com.ocp6.paymybuddy.dto.Subscriber;
import com.ocp6.paymybuddy.model.UserModel;
import com.ocp6.paymybuddy.repository.UserRepository;

@Service
public class AuthenticationService implements UserDetailsService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthenticationService.class);
    @Autowired
    private UserRepository repository;

    public boolean verifyMail(String mail) {
        return this.repository.findByMail(mail).isPresent();
    }

    public void subscribe(Subscriber dto, PasswordEncoder encoder) throws DatabaseErrorException {
        try {
            UserModel model = new UserModel();
            model.setFirstName(dto.getFirstName());
            model.setLastName(dto.getLastName());
            model.setMail(dto.getMail());
            model.setWallet(BigDecimal.valueOf(0));
            model.setPassword(encoder.encode(dto.getPassword()));
            repository.save(model);

        } catch (Exception e) {
            String message = "An error has occured during account creation";
            LOGGER.error(message, e);
            throw new DatabaseErrorException(message);

        }
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<UserModel> userBox = repository.findByMail(username);

        if (!userBox.isPresent()) {
            throw new UsernameNotFoundException("Account not found");
        }
        return new User(userBox.get().getMail(), userBox.get().getPassword(),
                Collections.singleton(new SimpleGrantedAuthority("ROLE_USER")));
    }

}
