package com.ocp6.paymybuddy.services;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ocp6.paymybuddy.config.exceptions.AccountNotFoundException;
import com.ocp6.paymybuddy.config.exceptions.NoMoneyException;
import com.ocp6.paymybuddy.dto.CurrentUserDto;
import com.ocp6.paymybuddy.dto.TransactionDto;
import com.ocp6.paymybuddy.mappers.CurrentUserMapper;
import com.ocp6.paymybuddy.model.Transaction;
import com.ocp6.paymybuddy.model.UserModel;
import com.ocp6.paymybuddy.repository.TransactionRepository;
import com.ocp6.paymybuddy.repository.UserRepository;

@Service
public class TransactionService {

    public static final BigDecimal COMMISSION_PERCENTAGE = BigDecimal.valueOf(0.5);
    final String accountMessage = "The requested account could not be found";

    @Autowired
    TransactionRepository repository;

    @Autowired
    UserRepository userRepository;

    @Transactional
    public CurrentUserDto transferMoney(String mail, TransactionDto dto)
            throws AccountNotFoundException, NoMoneyException {
        Transaction transaction = new Transaction();
        transaction.setDate(LocalDateTime.now());
        transaction.setDescription(dto.getDescription());
        UserModel envoyeur;
        UserModel receveur;

        Optional<UserModel> optEnvoyeur = userRepository.findByMail(mail);
        Optional<UserModel> optReceveur = userRepository.findByMail(dto.getConnection().getMail());

        if (optEnvoyeur.isPresent()) {
            envoyeur = optEnvoyeur.get();
        } else {
            throw new AccountNotFoundException(accountMessage);
        }
        if (optReceveur.isPresent()) {
            receveur = optReceveur.get();
        } else {
            throw new AccountNotFoundException(accountMessage);
        }

        BigDecimal envoyeurwallet = envoyeur.getWallet();
        BigDecimal toApply = envoyeurwallet.subtract(applyCommission(dto.getMontant()));
        if (toApply.compareTo(BigDecimal.valueOf(0)) >= 0) {
            envoyeur.setWallet(toApply);
        } else {
            throw new NoMoneyException("You don't have enough money on your account to perform this action");
        }
        BigDecimal receveurwallet = receveur.getWallet();
        receveur.setWallet(receveurwallet.add(dto.getMontant()));

        transaction.setMontant(dto.getMontant());
        transaction.setEnvoyeur(envoyeur);
        transaction.setReceveur(receveur);

        userRepository.save(receveur);
        userRepository.save(envoyeur);

        repository.save(transaction);

        Optional<UserModel> optuser = userRepository.findByMail(mail);
        if (optuser.isPresent()) {
            return CurrentUserMapper.instance.UsertoCurrentUserDto(optuser.get());
        } else {
            throw new AccountNotFoundException(accountMessage);
        }

    }

    public BigDecimal calcCommission(BigDecimal amount) {
        return (COMMISSION_PERCENTAGE.divide(BigDecimal.valueOf(100))).multiply(amount);
    }

    public BigDecimal applyCommission(BigDecimal amount) {
        BigDecimal commission = calcCommission(amount);
        // TRANSFER commission to PayMyBuddy Bank account
        return amount.add(commission);

    }
}
