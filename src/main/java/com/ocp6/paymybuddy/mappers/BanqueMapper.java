package com.ocp6.paymybuddy.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.ocp6.paymybuddy.dto.BanqueDto;
import com.ocp6.paymybuddy.model.Banque;

@Mapper
public interface BanqueMapper {

    BanqueMapper instance = Mappers.getMapper(BanqueMapper.class);

    BanqueDto ModeltoDto(Banque banque);

    Banque DtotoModel(BanqueDto banqueDto);
}
