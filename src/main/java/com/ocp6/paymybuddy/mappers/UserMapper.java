package com.ocp6.paymybuddy.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.ocp6.paymybuddy.dto.UserDto;
import com.ocp6.paymybuddy.model.UserModel;

@Mapper
public interface UserMapper {

    UserMapper instance = Mappers.getMapper(UserMapper.class);

    UserDto UsertoUserDto(UserModel user);
}
