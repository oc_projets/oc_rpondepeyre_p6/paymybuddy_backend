package com.ocp6.paymybuddy.mappers;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.ocp6.paymybuddy.dto.BanqueDto;
import com.ocp6.paymybuddy.dto.CurrentUserDto;
import com.ocp6.paymybuddy.dto.TransactionDto;
import com.ocp6.paymybuddy.dto.UserDto;
import com.ocp6.paymybuddy.model.Banque;
import com.ocp6.paymybuddy.model.Transaction;
import com.ocp6.paymybuddy.model.UserModel;

@Mapper
public abstract class CurrentUserMapper {

    public static CurrentUserMapper instance = Mappers.getMapper(CurrentUserMapper.class);

    public CurrentUserDto UsertoCurrentUserDto(UserModel user) {
        if (user != null) {
            CurrentUserDto result = new CurrentUserDto();
            result.setId(user.getId());
            result.setMail(user.getMail());
            result.setLastName(user.getLastName());
            result.setFirstName(user.getFirstName());
            result.setWallet(user.getWallet());
            Set<UserDto> users = new HashSet<>();
            if (user.getRelations() != null) {
                for (UserModel var : user.getRelations()) {
                    users.add(UserMapper.instance.UsertoUserDto(var));
                }
            }
            result.setRelations(users);
            Set<BanqueDto> banques = new HashSet<>();
            if (user.getBanques() != null) {
                for (Banque var : user.getBanques()) {
                    banques.add(BanqueMapper.instance.ModeltoDto(var));
                }
            }
            result.setBanques(banques);
            Set<TransactionDto> transactions = new HashSet<>();
            if (user.getEnvoyees() != null) {
                for (Transaction var : user.getEnvoyees()) {
                    TransactionDto dto = new TransactionDto();
                    dto.setDate(var.getDate());
                    dto.setDescription(var.getDescription());
                    dto.setConnection(UserMapper.instance.UsertoUserDto(var.getReceveur()));
                    dto.setMontant(var.getMontant().multiply(BigDecimal.valueOf(-1)));
                    transactions.add(dto);
                }
            }
            if (user.getRecues() != null) {
                for (Transaction var : user.getRecues()) {
                    TransactionDto dto = new TransactionDto();
                    dto.setDate(var.getDate());
                    dto.setDescription(var.getDescription());
                    dto.setConnection(UserMapper.instance.UsertoUserDto(var.getEnvoyeur()));
                    dto.setMontant(var.getMontant());
                    transactions.add(dto);
                }
            }
            result.setTransactions(transactions);
            return result;
        } else {
            return null;
        }
    };
}
