package com.ocp6.paymybuddy.dto;

import java.math.BigDecimal;

public class TransferDto {

    BanqueDto bank;
    BigDecimal amount;

    public TransferDto() {
        // Empty Constructor
    }

    public BanqueDto getBank() {
        return this.bank;
    }

    public void setBank(BanqueDto bank) {
        this.bank = bank;
    }

    public BigDecimal getAmount() {
        return this.amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

}
