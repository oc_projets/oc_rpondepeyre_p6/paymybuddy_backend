package com.ocp6.paymybuddy.dto;

public class TokenVerify {
    private String token;
    private String mail;

    public String getToken() {
        return this.token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getMail() {
        return this.mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

}
