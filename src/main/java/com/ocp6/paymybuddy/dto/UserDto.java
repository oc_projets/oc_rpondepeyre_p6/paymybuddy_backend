package com.ocp6.paymybuddy.dto;

public class UserDto {

    private String mail;
    private String lastName;
    private String firstName;

    public UserDto() {
        // Empty Constructor
    }

    public String getMail() {
        return this.mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

}
