package com.ocp6.paymybuddy.dto;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class TransactionDto {

    private String description;
    private BigDecimal montant;
    private LocalDateTime date;
    private UserDto connection;

    public TransactionDto() {
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getMontant() {
        return this.montant;
    }

    public void setMontant(BigDecimal montant) {
        this.montant = montant;
    }

    public LocalDateTime getDate() {
        return this.date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public UserDto getConnection() {
        return this.connection;
    }

    public void setConnection(UserDto connection) {
        this.connection = connection;
    }

}
