package com.ocp6.paymybuddy.dto;

import java.math.BigDecimal;
import java.util.Set;

public class CurrentUserDto {

    private int id;

    private String mail;
    private String lastName;
    private String firstName;
    private BigDecimal wallet;
    private Set<UserDto> relations;
    private Set<BanqueDto> banques;
    private Set<TransactionDto> transactions;

    public CurrentUserDto() {
        // Empty Constructor
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMail() {
        return this.mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public BigDecimal getWallet() {
        return this.wallet;
    }

    public void setWallet(BigDecimal wallet) {
        this.wallet = wallet;
    }

    public Set<UserDto> getRelations() {
        return this.relations;
    }

    public void setRelations(Set<UserDto> relations) {
        this.relations = relations;
    }

    public Set<BanqueDto> getBanques() {
        return this.banques;
    }

    public void setBanques(Set<BanqueDto> banques) {
        this.banques = banques;
    }

    public Set<TransactionDto> getTransactions() {
        return this.transactions;
    }

    public void setTransactions(Set<TransactionDto> transactions) {
        this.transactions = transactions;
    }
}
