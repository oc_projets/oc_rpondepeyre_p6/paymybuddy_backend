package com.ocp6.paymybuddy.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity(name = "Transaction")
@Table(schema = "paymybuddy", name = "transaction")
public class Transaction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "description")
    private String description;

    @Column(name = "montant")
    private BigDecimal montant;

    @Column(name = "date")
    private LocalDateTime date;

    @ManyToOne
    @JoinColumn(name = "envoyeur_id")
    private UserModel envoyeur;

    @ManyToOne
    @JoinColumn(name = "receveur_id")
    private UserModel receveur;

    public Transaction() {
        // Constructeur
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getMontant() {
        return this.montant;
    }

    public void setMontant(BigDecimal montant) {
        this.montant = montant;
    }

    public LocalDateTime getDate() {
        return this.date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public UserModel getEnvoyeur() {
        return this.envoyeur;
    }

    public void setEnvoyeur(UserModel envoyeur) {
        this.envoyeur = envoyeur;
    }

    public UserModel getReceveur() {
        return this.receveur;
    }

    public void setReceveur(UserModel receveur) {
        this.receveur = receveur;
    }

}
