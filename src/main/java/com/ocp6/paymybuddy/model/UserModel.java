package com.ocp6.paymybuddy.model;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

@Entity(name = "Utilisateur")
@Table(schema = "paymybuddy", name = "utilisateur")
public class UserModel implements UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "mail", nullable = false, unique = true)
    private String mail;
    @Column(name = "password", nullable = false)
    private String password;
    @Column(name = "nom", nullable = false)
    private String lastName;
    @Column(name = "prenom", nullable = false)
    private String firstName;
    @Column(name = "wallet", nullable = false)
    private BigDecimal wallet;

    @ManyToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
    @JoinTable(schema = "paymybuddy", name = "relation", joinColumns = @JoinColumn(name = "ajouteur_id"), inverseJoinColumns = {
            @JoinColumn(name = "ajouted_id") })
    private Set<UserModel> relations;

    @OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.EAGER, mappedBy = "utilisateur", orphanRemoval = true)
    private Set<Banque> banques;

    @OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "envoyeur")
    private List<Transaction> envoyees;

    @OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "receveur")
    private List<Transaction> recues;

    public UserModel() {
        // Constructeur
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMail() {
        return this.mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public BigDecimal getWallet() {
        return this.wallet;
    }

    public void setWallet(BigDecimal wallet) {
        this.wallet = wallet;
    }

    public Set<UserModel> getRelations() {
        return this.relations;
    }

    public void setRelations(Set<UserModel> relations) {
        this.relations = relations;
    }

    public Set<Banque> getBanques() {
        return this.banques;
    }

    public void setBanques(Set<Banque> banques) {
        this.banques = banques;
    }

    public List<Transaction> getEnvoyees() {
        return this.envoyees;
    }

    public void setEnvoyees(List<Transaction> envoyees) {
        this.envoyees = envoyees;
    }

    public List<Transaction> getRecues() {
        return this.recues;
    }

    public void setRecues(List<Transaction> recues) {
        this.recues = recues;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.emptyList();
    }

    @Override
    public String getUsername() {
        return this.mail;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {

        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

}
