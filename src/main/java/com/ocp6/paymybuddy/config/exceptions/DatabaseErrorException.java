package com.ocp6.paymybuddy.config.exceptions;

public class DatabaseErrorException extends Exception {

    public DatabaseErrorException(String error) {
        super(error);
    }
}
