package com.ocp6.paymybuddy.config.exceptions;

public class NoMoneyException extends Exception {

    public NoMoneyException(String error) {
        super(error);
    }
}
