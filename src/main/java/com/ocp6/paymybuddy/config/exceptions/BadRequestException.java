package com.ocp6.paymybuddy.config.exceptions;

public class BadRequestException extends Exception {

    public BadRequestException(String error) {
        super(error);
    }
}
