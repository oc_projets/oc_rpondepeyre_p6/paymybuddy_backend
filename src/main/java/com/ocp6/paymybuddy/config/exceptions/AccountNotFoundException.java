package com.ocp6.paymybuddy.config.exceptions;

public class AccountNotFoundException extends Exception {

    public AccountNotFoundException(String error) {
        super(error);
    }
}
