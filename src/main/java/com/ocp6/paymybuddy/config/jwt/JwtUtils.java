package com.ocp6.paymybuddy.config.jwt;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import com.ocp6.paymybuddy.config.exceptions.AccountNotFoundException;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class JwtUtils {

    private static final String SECRET = "secret";
    private static final int TOKEN_VALID_INHOURS = 2;

    private Claims extractAllClaims(String token) {
        return Jwts.parser().setSigningKey(SECRET).parseClaimsJws(token).getBody();
    }

    public String getUsernamefromToken(String token) {
        return extractAllClaims(token).getSubject();
    }

    public Date getExpirationFromToken(String token) {
        return extractAllClaims(token).getExpiration();
    }

    public Boolean isTokenExpired(String token) {
        return getExpirationFromToken(token).before(new Date());
    }

    public String generateToken(UserDetails userDetails) {
        Map<String, Object> claims = new HashMap<>();

        return Jwts.builder()
                .setClaims(claims)
                .setSubject(userDetails.getUsername())
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + TimeUnit.HOURS.toMillis(TOKEN_VALID_INHOURS)))
                .signWith(SignatureAlgorithm.HS256, SECRET).compact();
    }

    public Boolean validateToken(String token, UserDetails userDetails) {
        return (this.getUsernamefromToken(token).equals(userDetails.getUsername())) && !isTokenExpired(token);
    }

    public String getTokenFromHeader(String authHeader) throws AccountNotFoundException {
        if (authHeader.startsWith("Bearer ")) {
            return authHeader.substring(7, authHeader.length());
        } else {
            throw new AccountNotFoundException("Bad format token");
        }
    }
}
