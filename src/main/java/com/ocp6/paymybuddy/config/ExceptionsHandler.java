package com.ocp6.paymybuddy.config;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.ocp6.paymybuddy.config.exceptions.AccountNotFoundException;
import com.ocp6.paymybuddy.config.exceptions.BadRequestException;
import com.ocp6.paymybuddy.config.exceptions.DatabaseErrorException;
import com.ocp6.paymybuddy.config.exceptions.NoMoneyException;

@ControllerAdvice
public class ExceptionsHandler {

    @ExceptionHandler(value = DatabaseErrorException.class)
    public ResponseEntity<String> databaseErrorException(DatabaseErrorException e) {
        String error = e.getMessage();
        return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(error);
    }

    @ExceptionHandler(value = BadRequestException.class)
    public ResponseEntity<String> BadRequestException(BadRequestException e) {
        String error = e.getMessage();
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(error);
    }

    @ExceptionHandler(value = AccountNotFoundException.class)
    public ResponseEntity<String> AccountNotFoundException(AccountNotFoundException e) {
        String error = e.getMessage();
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(error);
    }

    @ExceptionHandler(value = NoMoneyException.class)
    public ResponseEntity<String> NoMoneyException(NoMoneyException e) {
        String error = e.getMessage();
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(error);
    }
}
