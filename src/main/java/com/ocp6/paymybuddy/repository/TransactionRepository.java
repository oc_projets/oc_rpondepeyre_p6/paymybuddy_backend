package com.ocp6.paymybuddy.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ocp6.paymybuddy.model.Transaction;

public interface TransactionRepository extends JpaRepository<Transaction, Integer> {

}
