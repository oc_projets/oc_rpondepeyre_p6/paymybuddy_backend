package com.ocp6.paymybuddy.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ocp6.paymybuddy.model.Banque;

public interface BanqueRepository extends JpaRepository<Banque, Integer> {

}
