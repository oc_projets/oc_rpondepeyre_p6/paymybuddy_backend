package com.ocp6.paymybuddy.servicesTests;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.ocp6.paymybuddy.config.exceptions.AccountNotFoundException;
import com.ocp6.paymybuddy.config.exceptions.NoMoneyException;
import com.ocp6.paymybuddy.dto.TransactionDto;
import com.ocp6.paymybuddy.dto.UserDto;
import com.ocp6.paymybuddy.model.Transaction;
import com.ocp6.paymybuddy.model.UserModel;
import com.ocp6.paymybuddy.repository.TransactionRepository;
import com.ocp6.paymybuddy.repository.UserRepository;
import com.ocp6.paymybuddy.services.TransactionService;

@ExtendWith(MockitoExtension.class)
public class TransactionServiceTests {

        final String accountMessage = "The requested account could not be found";

        @Mock
        TransactionRepository repository;

        @Mock
        UserRepository userRepository;

        @InjectMocks
        TransactionService service;

        @Test
        public void transferMoney_shouldCreateTransaction() throws AccountNotFoundException, NoMoneyException {
                UserDto receveurdto = new UserDto();
                receveurdto.setMail("receveur@mail.fr");

                TransactionDto transaction = new TransactionDto();
                transaction.setConnection(receveurdto);
                transaction.setDescription("description");
                transaction.setMontant(BigDecimal.valueOf(5));

                UserModel envoyeur = new UserModel();
                envoyeur.setWallet(BigDecimal.valueOf(10));
                envoyeur.setMail("envoyeur@mail.fr");
                envoyeur.setEnvoyees(new ArrayList<Transaction>());

                UserModel receveur = new UserModel();
                receveur.setWallet(BigDecimal.valueOf(10));
                receveur.setMail("receveur@mail.fr");
                receveur.setRecues(new ArrayList<Transaction>());

                Optional<UserModel> optEnvoyeur = Optional.of(envoyeur);
                Optional<UserModel> optReceveur = Optional.of(receveur);

                when(userRepository.findByMail("envoyeur@mail.fr")).thenReturn(optEnvoyeur);
                when(userRepository.findByMail("receveur@mail.fr")).thenReturn(optReceveur);

                ArgumentCaptor<Transaction> argtransaction = ArgumentCaptor.forClass(Transaction.class);
                ArgumentCaptor<UserModel> arguser = ArgumentCaptor.forClass(UserModel.class);

                service.transferMoney("envoyeur@mail.fr", transaction);

                verify(userRepository, Mockito.times(2)).save(arguser.capture());
                verify(repository).save(argtransaction.capture());

                List<UserModel> users = arguser.getAllValues();
                assertThat(users.get(0).getWallet()).isEqualTo(BigDecimal.valueOf(15));
                assertThat(users.get(1).getWallet()).isEqualTo(BigDecimal.valueOf(4.975));

                Transaction saved = argtransaction.getValue();
                assertThat(saved.getDescription()).isEqualTo("description");
                assertThat(saved.getMontant()).isEqualTo(BigDecimal.valueOf(5));
                assertThat(saved.getEnvoyeur()).isEqualToComparingFieldByField(envoyeur);
                assertThat(saved.getReceveur()).isEqualToComparingFieldByField(receveur);
        }

        @Test
        public void transferMoney_shouldThrowError_whenEnvoyeurisIncorrect()
                        throws AccountNotFoundException, NoMoneyException {

                UserDto receveurdto = new UserDto();
                receveurdto.setMail("receveur@mail.fr");

                TransactionDto transaction = new TransactionDto();
                transaction.setConnection(receveurdto);
                transaction.setDescription("description");
                transaction.setMontant(BigDecimal.valueOf(5));

                UserModel receveur = new UserModel();
                receveur.setWallet(BigDecimal.valueOf(10));
                receveur.setMail("receveur@mail.fr");
                receveur.setRecues(new ArrayList<Transaction>());

                Optional<UserModel> optReceveur = Optional.of(receveur);

                when(userRepository.findByMail("envoyeur@mail.fr")).thenReturn(Optional.empty());
                when(userRepository.findByMail("receveur@mail.fr")).thenReturn(optReceveur);

                AccountNotFoundException thrown = assertThrows(AccountNotFoundException.class,
                                () -> service.transferMoney("envoyeur@mail.fr", transaction));

                assertThat(thrown.getMessage()).isEqualToIgnoringCase(accountMessage);
        }

        @Test
        public void transferMoney_shouldThrowError_whenreceveurisIncorrect()
                        throws AccountNotFoundException, NoMoneyException {

                UserDto receveurdto = new UserDto();
                receveurdto.setMail("receveur@mail.fr");

                TransactionDto transaction = new TransactionDto();
                transaction.setConnection(receveurdto);
                transaction.setDescription("description");
                transaction.setMontant(BigDecimal.valueOf(5));

                UserModel envoyeur = new UserModel();
                envoyeur.setWallet(BigDecimal.valueOf(10));
                envoyeur.setMail("receveur@mail.fr");
                envoyeur.setRecues(new ArrayList<Transaction>());

                Optional<UserModel> optenvoyeur = Optional.of(envoyeur);

                when(userRepository.findByMail("envoyeur@mail.fr")).thenReturn(optenvoyeur);
                when(userRepository.findByMail("receveur@mail.fr")).thenReturn(Optional.empty());

                AccountNotFoundException thrown = assertThrows(AccountNotFoundException.class,
                                () -> service.transferMoney("envoyeur@mail.fr", transaction));

                assertThat(thrown.getMessage()).isEqualToIgnoringCase(accountMessage);
        }

        @Test
        public void transferMoney_shouldThrowErrorWhenUserHasNotEnoughMoney()
                        throws AccountNotFoundException, NoMoneyException {
                UserDto receveurdto = new UserDto();
                receveurdto.setMail("receveur@mail.fr");

                TransactionDto transaction = new TransactionDto();
                transaction.setConnection(receveurdto);
                transaction.setDescription("description");
                transaction.setMontant(BigDecimal.valueOf(5));

                UserModel envoyeur = new UserModel();
                envoyeur.setWallet(BigDecimal.valueOf(1));
                envoyeur.setMail("envoyeur@mail.fr");
                envoyeur.setEnvoyees(new ArrayList<Transaction>());

                UserModel receveur = new UserModel();
                receveur.setWallet(BigDecimal.valueOf(10));
                receveur.setMail("receveur@mail.fr");
                receveur.setRecues(new ArrayList<Transaction>());

                Optional<UserModel> optEnvoyeur = Optional.of(envoyeur);
                Optional<UserModel> optReceveur = Optional.of(receveur);

                when(userRepository.findByMail("envoyeur@mail.fr")).thenReturn(optEnvoyeur);
                when(userRepository.findByMail("receveur@mail.fr")).thenReturn(optReceveur);

                NoMoneyException thrown = assertThrows(NoMoneyException.class,
                                () -> service.transferMoney("envoyeur@mail.fr", transaction));

                assertThat(thrown.getMessage())
                                .isEqualToIgnoringCase(
                                                "You don't have enough money on your account to perform this action");

        }
}
