package com.ocp6.paymybuddy.servicesTests;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

import java.math.BigDecimal;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.ocp6.paymybuddy.config.exceptions.DatabaseErrorException;
import com.ocp6.paymybuddy.dto.Subscriber;
import com.ocp6.paymybuddy.model.UserModel;
import com.ocp6.paymybuddy.repository.UserRepository;
import com.ocp6.paymybuddy.services.AuthenticationService;

import nl.altindag.log.LogCaptor;

@ExtendWith(MockitoExtension.class)
public class AuthenticationServiceTests {

    @Mock
    UserRepository repository;

    @InjectMocks
    AuthenticationService service;

    LogCaptor logCaptor = LogCaptor.forClass(AuthenticationService.class);

    @Test
    public void verifyMail_WhenMailExists_ShouldReturnTrue() {
        // Arrange
        String mail = "test@test.com";
        doReturn(Optional.of(new UserModel())).when(repository).findByMail(mail);
        // Act
        boolean result = service.verifyMail(mail);

        // Assert
        assertTrue(result);
    }

    @Test
    public void verifyMail_WhenMailDoesntExists_ShouldReturnFalse() {
        // Arrange
        String mail = "test@test.com";
        doReturn(Optional.empty()).when(repository).findByMail(mail);
        // Act
        boolean result = service.verifyMail(mail);

        // Assert
        assertFalse(result);
    }

    @Test
    public void subscribe_shouldCreateUserModel() throws DatabaseErrorException {
        Subscriber dto = new Subscriber();
        dto.setFirstName("John");
        dto.setLastName("Doe");
        dto.setMail("john.doe@example.com");
        dto.setPassword("password");
        PasswordEncoder encoder = Mockito.mock(PasswordEncoder.class);

        UserModel expected = new UserModel();
        expected.setFirstName(dto.getFirstName());
        expected.setLastName(dto.getLastName());
        expected.setMail(dto.getMail());
        expected.setWallet(BigDecimal.valueOf(0));
        expected.setPassword("encoded_password");

        Mockito.when(encoder.encode(dto.getPassword())).thenReturn("encoded_password");
        ArgumentCaptor<UserModel> argument = ArgumentCaptor.forClass(UserModel.class);

        service.subscribe(dto, encoder);
        verify(repository).save(argument.capture());
        assertThat(argument.getValue()).isEqualToComparingFieldByField(expected);

    }

    @Test
    public void subscribeWithnull_shouldThrowError() throws DatabaseErrorException {

        String message = "An error has occured during account creation";

        PasswordEncoder encoder = Mockito.mock(PasswordEncoder.class);
        DatabaseErrorException thrown = assertThrows(DatabaseErrorException.class,
                () -> service.subscribe(null, encoder));

        assertThat(logCaptor.getErrorLogs()).containsExactly(message);
        assertThat(thrown.getMessage()).isEqualToIgnoringCase(message);

    }

    @Test
    public void loadUserByUsername_whenUserExists_shouldReturnUserDetails() {

        String username = "test@example.com";
        UserModel userModel = new UserModel();
        userModel.setMail(username);
        userModel.setPassword("password");
        Mockito.when(repository.findByMail(username)).thenReturn(Optional.of(userModel));

        UserDetails result = service.loadUserByUsername(username);

        assertNotNull(result);
        assertEquals("test@example.com", result.getUsername());
        assertEquals("password", result.getPassword());

    }

    @Test
    public void loadUserByUsername_whenUserDoesntExists_shouldThrowError() {

        String username = "test@example.com";
        Mockito.when(repository.findByMail(username)).thenReturn(Optional.empty());

        UsernameNotFoundException thrown = assertThrows(UsernameNotFoundException.class,
                () -> service.loadUserByUsername(username));

        assertThat(thrown.getMessage()).isEqualToIgnoringCase("Account not found");
    }
}
