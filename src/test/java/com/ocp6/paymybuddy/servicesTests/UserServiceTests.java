package com.ocp6.paymybuddy.servicesTests;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.ocp6.paymybuddy.config.exceptions.AccountNotFoundException;
import com.ocp6.paymybuddy.config.exceptions.BadRequestException;
import com.ocp6.paymybuddy.config.exceptions.NoMoneyException;
import com.ocp6.paymybuddy.dto.BanqueDto;
import com.ocp6.paymybuddy.dto.CurrentUserDto;
import com.ocp6.paymybuddy.dto.TransferDto;
import com.ocp6.paymybuddy.dto.UserDto;
import com.ocp6.paymybuddy.model.Banque;
import com.ocp6.paymybuddy.model.Transaction;
import com.ocp6.paymybuddy.model.UserModel;
import com.ocp6.paymybuddy.repository.UserRepository;
import com.ocp6.paymybuddy.services.UserService;

@ExtendWith(MockitoExtension.class)
public class UserServiceTests {

    final String accountMessage = "The requested account could not be found";

    @Mock
    UserRepository repository;

    @InjectMocks
    UserService service;

    @Test
    public void findCurrentUserByMail_shouldReturnCurrentUserDto() throws AccountNotFoundException {

        UserModel user = new UserModel();
        String mail = "test@example.com";
        user.setMail(mail);
        user.setFirstName("firstname");
        user.setLastName("lastname");
        user.setWallet(BigDecimal.valueOf(5));

        UserModel relation = new UserModel();
        Set<UserModel> relations = new HashSet<>();
        relations.add(relation);
        user.setRelations(relations);

        Banque banque = new Banque();
        Set<Banque> banques = new HashSet<>();
        banques.add(banque);
        user.setBanques(banques);

        Transaction transaction = new Transaction();
        transaction.setMontant(BigDecimal.valueOf(1));
        List<Transaction> transactions = new ArrayList<>();
        transactions.add(transaction);

        user.setRecues(transactions);
        user.setEnvoyees(transactions);

        when(repository.findByMail(mail)).thenReturn(Optional.of(user));

        CurrentUserDto result = service.findCurrentUserByMail(mail);

        assertNotNull(result);
        assertThat(result).isInstanceOf(CurrentUserDto.class);
        assertThat(result.getMail()).isEqualTo(mail);
    }

    @Test
    public void findCurrentUserByMail_shouldThrowError() {

        String mail = "test@mail.com";
        when(repository.findByMail(mail)).thenReturn(Optional.empty());

        AccountNotFoundException thrown = assertThrows(AccountNotFoundException.class,
                () -> service.findCurrentUserByMail(mail));

        assertThat(thrown.getMessage()).isEqualToIgnoringCase(accountMessage);
    }

    @Test
    public void findUserByMail_shouldReturnUserDto() throws AccountNotFoundException {

        UserModel user = new UserModel();
        String mail = "test@example.com";
        user.setMail(mail);
        when(repository.findByMail(mail)).thenReturn(Optional.of(user));

        UserDto result = service.findUserByMail(mail);

        assertNotNull(result);
        assertThat(result).isInstanceOf(UserDto.class);
        assertThat(result.getMail()).isEqualTo(mail);
    }

    @Test
    public void findUserByMail_shouldThrowError() {

        String mail = "test@mail.com";
        when(repository.findByMail(mail)).thenReturn(Optional.empty());

        AccountNotFoundException thrown = assertThrows(AccountNotFoundException.class,
                () -> service.findUserByMail(mail));

        assertThat(thrown.getMessage()).isEqualToIgnoringCase(accountMessage);
    }

    @Test
    public void addConnection_ShouldReturnUserDto_WhenValidInputs()
            throws BadRequestException, AccountNotFoundException {

        String currentUserMail = "test@mail.com";
        String addUserMail = "test2@mail.com";

        UserModel currentUserModel = new UserModel();
        currentUserModel.setMail(currentUserMail);
        currentUserModel.setRelations(new HashSet<>());

        UserModel addUserModel = new UserModel();
        addUserModel.setMail(addUserMail);

        when(repository.findByMail(currentUserMail)).thenReturn(Optional.of(currentUserModel));
        when(repository.findByMail(addUserMail)).thenReturn(Optional.of(addUserModel));

        service.addConnection(currentUserMail, addUserMail);
        ArgumentCaptor<UserModel> argument = ArgumentCaptor.forClass(UserModel.class);

        verify(repository, Mockito.times(1)).save(argument.capture());
        assertThat(argument.getValue().getRelations()).hasSize(1);
        assertThat(argument.getValue().getMail()).isEqualToIgnoringCase(currentUserMail);
        assertThat(argument.getValue().getRelations().iterator().next().getMail()).isEqualToIgnoringCase(addUserMail);
    }

    @Test
    public void addConnection_ShouldThrowBadRequestException_WhenCurrentAndAddUsersAreTheSame()
            throws BadRequestException {
        String currentAndAddUsersAreTheSame = "test@mail.com";

        BadRequestException thrown = assertThrows(BadRequestException.class,
                () -> service.addConnection(currentAndAddUsersAreTheSame, currentAndAddUsersAreTheSame));

        assertThat(thrown.getMessage()).isEqualToIgnoringCase("You cannot add yourself as friend");
    }

    @Test
    public void addConnection_ShouldThrowBadRequestException_WhenAddUserDoesntExist()
            throws BadRequestException {
        String currentUsermail = "john@mail.com";
        String addUsermail = "jane@mail.com";
        UserModel currentUser = new UserModel();
        currentUser.setMail(currentUsermail);

        doReturn(Optional.of(currentUser)).when(repository).findByMail(currentUsermail);
        doReturn(Optional.empty()).when(repository).findByMail(addUsermail);

        BadRequestException thrown = assertThrows(BadRequestException.class,
                () -> service.addConnection(currentUsermail, addUsermail));

        assertThat(thrown.getMessage()).isEqualToIgnoringCase("User not found with this mail");
    }

    @Test
    public void addConnection_ShouldThrowBadRequestException_WhenAddUserisAlreadyinConnectionsList()
            throws BadRequestException {
        String currentUsermail = "john@mail.com";
        String addUsermail = "jane@mail.com";
        UserModel currentUser = new UserModel();
        currentUser.setMail(currentUsermail);
        UserModel addUser = new UserModel();
        addUser.setMail(addUsermail);
        Set<UserModel> currentUserrelations = new HashSet<>();
        currentUserrelations.add(addUser);
        currentUser.setRelations(currentUserrelations);

        doReturn(Optional.of(currentUser)).when(repository).findByMail(currentUsermail);
        doReturn(Optional.of(addUser)).when(repository).findByMail(addUsermail);

        BadRequestException thrown = assertThrows(BadRequestException.class,
                () -> service.addConnection(currentUsermail, addUsermail));

        assertThat(thrown.getMessage()).isEqualToIgnoringCase("This User is already in your connections list");
    }

    @Test
    public void addConnection_ShouldThrowAccountNotFoundException()
            throws BadRequestException {
        String currentUsermail = "john@mail.com";
        String addUsermail = "jane@mail.com";
        UserModel currentUser = new UserModel();
        currentUser.setMail(currentUsermail);
        UserModel addUser = new UserModel();
        addUser.setMail(addUsermail);

        doReturn(Optional.empty()).when(repository).findByMail(currentUsermail);
        doReturn(Optional.of(addUser)).when(repository).findByMail(addUsermail);

        AccountNotFoundException thrown = assertThrows(AccountNotFoundException.class,
                () -> service.addConnection(currentUsermail, addUsermail));

        assertThat(thrown.getMessage()).isEqualToIgnoringCase(accountMessage);
    }

    @Test
    public void removeConnection_WhenValidInput_ShouldReturnUpdatedUserDto() throws AccountNotFoundException {

        String currentUserMail = "test@gmail.com";
        String removeUserMail = "remove@gmail.com";

        UserModel currentUser = new UserModel();
        currentUser.setMail(currentUserMail);

        UserModel addUser = new UserModel();
        addUser.setMail(removeUserMail);

        Set<UserModel> relations = new HashSet<>();
        relations.add(addUser);

        currentUser.setRelations(relations);

        when(repository.findByMail(currentUserMail)).thenReturn(Optional.of(currentUser));
        when(repository.findByMail(removeUserMail)).thenReturn(Optional.of(addUser));
        ArgumentCaptor<UserModel> argument = ArgumentCaptor.forClass(UserModel.class);

        service.removeConnection(currentUserMail, removeUserMail);

        verify(repository).save(argument.capture());
        assertThat(argument.getValue().getRelations()).isEmpty();
        assertThat(argument.getValue().getMail()).isEqualToIgnoringCase(currentUserMail);
    }

    @Test
    public void removeConnection_ShouldThrowAccountNotFoundExceptionWhenCurrentUserisEmpty()
            throws BadRequestException {
        String currentUsermail = "john@mail.com";
        String addUsermail = "jane@mail.com";
        UserModel currentUser = new UserModel();
        currentUser.setMail(currentUsermail);
        UserModel addUser = new UserModel();
        addUser.setMail(addUsermail);

        doReturn(Optional.empty()).when(repository).findByMail(currentUsermail);
        doReturn(Optional.of(addUser)).when(repository).findByMail(addUsermail);

        AccountNotFoundException thrown = assertThrows(AccountNotFoundException.class,
                () -> service.removeConnection(currentUsermail, addUsermail));

        assertThat(thrown.getMessage()).isEqualToIgnoringCase(accountMessage);
    }

    @Test
    public void removeConnection_ShouldThrowAccountNotFoundExceptionWhenAddUserisEmpty()
            throws BadRequestException {
        String currentUsermail = "john@mail.com";
        String addUsermail = "jane@mail.com";
        UserModel currentUser = new UserModel();
        currentUser.setMail(currentUsermail);
        UserModel addUser = new UserModel();
        addUser.setMail(addUsermail);

        doReturn(Optional.of(currentUser)).when(repository).findByMail(currentUsermail);
        doReturn(Optional.empty()).when(repository).findByMail(addUsermail);

        AccountNotFoundException thrown = assertThrows(AccountNotFoundException.class,
                () -> service.removeConnection(currentUsermail, addUsermail));

        assertThat(thrown.getMessage()).isEqualToIgnoringCase(accountMessage);
    }

    @Test
    public void relationExists_whenRelationExists_returnsTrue() {

        UserModel currentUser = new UserModel();
        currentUser.setMail("john@example.com");
        UserModel addUser = new UserModel();
        addUser.setMail("jane@example.com");

        Set<UserModel> relations = new HashSet<UserModel>();
        relations.add(addUser);
        currentUser.setRelations(relations);

        boolean result = service.relationExists(currentUser, addUser);

        assertTrue(result);
    }

    @Test
    public void relationExists_whenRelationDoesNotExist_returnsFalse() {

        UserModel currentUser = new UserModel();
        currentUser.setMail("john@example.com");
        currentUser.setRelations(new HashSet<UserModel>());
        UserModel addUser = new UserModel();
        addUser.setMail("jane@example.com");

        boolean result = service.relationExists(currentUser, addUser);

        assertFalse(result);
    }

    @Test
    public void addBankAccount_ShouldsavenewUser_WhenValidInput() throws AccountNotFoundException {

        String mail = "test@test.com";
        UserModel user = new UserModel();
        user.setMail(mail);
        user.setId(1);
        user.setBanques(new HashSet<Banque>());

        BanqueDto banque = new BanqueDto();
        banque.setBic("Bic");
        banque.setIban("Iban");
        banque.setNom("nom");

        when(repository.findByMail(mail)).thenReturn(Optional.of(user));
        when(repository.save(any())).thenReturn(new UserModel());

        ArgumentCaptor<UserModel> argument = ArgumentCaptor.forClass(UserModel.class);

        service.addBankAccount(mail, banque);

        verify(repository).save(argument.capture());
        assertThat(argument.getValue().getBanques()).hasSize(1);
        assertThat(argument.getValue().getBanques().iterator().next().getNom()).isEqualToIgnoringCase("nom");
    }

    @Test
    public void addBankAccount_ShouldThrowExceptiont() throws AccountNotFoundException {

        String mail = "test@test.com";

        BanqueDto banque = new BanqueDto();
        banque.setBic("Bic");
        banque.setIban("Iban");
        banque.setNom("nom");

        when(repository.findByMail(mail)).thenReturn(Optional.empty());

        AccountNotFoundException thrown = assertThrows(AccountNotFoundException.class,
                () -> service.addBankAccount(mail, banque));

        assertThat(thrown.getMessage()).isEqualToIgnoringCase(accountMessage);
    }

    @Test
    public void removeBankAccount_ShouldsavenewUser_WhenValidInput() throws AccountNotFoundException {

        String mail = "test@test.com";
        UserModel user = new UserModel();
        user.setMail(mail);
        user.setId(1);

        Banque banque = new Banque();
        banque.setId(1);
        banque.setBic("Bic");
        banque.setIban("Iban");
        banque.setNom("nom");

        Banque banque2 = new Banque();
        banque.setId(2);
        banque.setBic("OTHERBic");
        banque.setIban("OTHERIban");
        banque.setNom("OTHERnom");

        Set<Banque> banques = new HashSet<>();
        banques.add(banque);
        banques.add(banque2);

        user.setBanques(banques);

        when(repository.findByMail(mail)).thenReturn(Optional.of(user));
        when(repository.save(any())).thenReturn(new UserModel());

        ArgumentCaptor<UserModel> argument = ArgumentCaptor.forClass(UserModel.class);

        service.removeBankAccount(mail, 2);

        verify(repository).save(argument.capture());
        assertThat(argument.getValue().getBanques()).hasSize(1);
    }

    @Test
    public void removeBankAccount_ShouldThrowExceptiont() throws AccountNotFoundException {

        String mail = "test@test.com";

        when(repository.findByMail(mail)).thenReturn(Optional.empty());

        AccountNotFoundException thrown = assertThrows(AccountNotFoundException.class,
                () -> service.removeBankAccount(mail, 1));

        assertThat(thrown.getMessage()).isEqualToIgnoringCase(accountMessage);
    }

    @Test
    public void removeBankAccount_ShouldthrowErrorwhenBankisnotfound() throws AccountNotFoundException {

        String mail = "test@test.com";
        UserModel user = new UserModel();
        user.setMail(mail);
        user.setId(1);
        user.setBanques(new HashSet<>());

        when(repository.findByMail(mail)).thenReturn(Optional.of(user));

        AccountNotFoundException thrown = assertThrows(AccountNotFoundException.class,
                () -> service.removeBankAccount(mail, 1));

        assertThat(thrown.getMessage()).isEqualToIgnoringCase("No bank account founded with specified informations");
    }

    @Test
    public void deposit_shouldaddMoneytoWallet() throws AccountNotFoundException {

        String mail = "test@test.com";
        UserModel user = new UserModel();
        user.setMail(mail);
        user.setId(1);
        user.setWallet(BigDecimal.valueOf(12));

        Banque banque = new Banque();
        banque.setId(1);
        banque.setBic("Bic");
        banque.setIban("Iban");
        banque.setNom("nom");

        Set<Banque> banques = new HashSet<>();
        banques.add(banque);

        user.setBanques(banques);

        BanqueDto banquedto = new BanqueDto();
        banquedto.setId(1);
        banquedto.setBic("Bic");
        banquedto.setIban("Iban");
        banquedto.setNom("nom");

        TransferDto transfer = new TransferDto();
        transfer.setBank(banquedto);
        transfer.setAmount(BigDecimal.valueOf(12));

        when(repository.findByMail(mail)).thenReturn(Optional.of(user));
        when(repository.save(any())).thenReturn(new UserModel());

        ArgumentCaptor<UserModel> argument = ArgumentCaptor.forClass(UserModel.class);

        service.deposit(mail, transfer);

        verify(repository).save(argument.capture());
        assertThat(argument.getValue().getWallet()).isEqualTo(BigDecimal.valueOf(24));

    }

    @Test
    public void deposit_ShouldThrowExceptiont() throws AccountNotFoundException {

        String mail = "test@test.com";
        TransferDto transfer = new TransferDto();

        when(repository.findByMail(mail)).thenReturn(Optional.empty());

        AccountNotFoundException thrown = assertThrows(AccountNotFoundException.class,
                () -> service.deposit(mail, transfer));

        assertThat(thrown.getMessage()).isEqualToIgnoringCase(accountMessage);
    }

    @Test
    public void deposit_shouldThrowErrorwhenBankaccountisinvalid() throws AccountNotFoundException {

        String mail = "test@test.com";
        UserModel user = new UserModel();
        user.setMail(mail);
        user.setId(1);
        user.setWallet(BigDecimal.valueOf(12));

        Banque banque = new Banque();
        banque.setId(1);
        banque.setBic("Bic");
        banque.setIban("Iban");
        banque.setNom("nom");

        Set<Banque> banques = new HashSet<>();
        banques.add(banque);

        user.setBanques(banques);

        BanqueDto banquedto = new BanqueDto();
        banquedto.setId(1);
        banquedto.setBic("Error");
        banquedto.setIban("Iban");
        banquedto.setNom("nom");

        TransferDto transfer = new TransferDto();
        transfer.setBank(banquedto);
        transfer.setAmount(BigDecimal.valueOf(12));
        when(repository.findByMail(mail)).thenReturn(Optional.of(user));

        AccountNotFoundException thrown = assertThrows(AccountNotFoundException.class,
                () -> service.deposit(mail, transfer));

        assertThat(thrown.getMessage()).isEqualToIgnoringCase("No bank account founded with specified informations");
    }

    @Test
    public void deposit_shouldThrowErrorwhenBankaccountisinvalid2() throws AccountNotFoundException {

        String mail = "test@test.com";
        UserModel user = new UserModel();
        user.setMail(mail);
        user.setId(1);
        user.setWallet(BigDecimal.valueOf(12));

        Banque banque = new Banque();
        banque.setId(1);
        banque.setBic("Bic");
        banque.setIban("Iban");
        banque.setNom("nom");

        Set<Banque> banques = new HashSet<>();
        banques.add(banque);

        user.setBanques(banques);

        BanqueDto banquedto = new BanqueDto();
        banquedto.setId(1);
        banquedto.setBic("Bic");
        banquedto.setIban("Error");
        banquedto.setNom("nom");

        TransferDto transfer = new TransferDto();
        transfer.setBank(banquedto);
        transfer.setAmount(BigDecimal.valueOf(12));
        when(repository.findByMail(mail)).thenReturn(Optional.of(user));

        AccountNotFoundException thrown = assertThrows(AccountNotFoundException.class,
                () -> service.deposit(mail, transfer));

        assertThat(thrown.getMessage()).isEqualToIgnoringCase("No bank account founded with specified informations");
    }

    @Test
    public void deposit_shouldThrowErrorwhenBankaccountisinvalid3() throws AccountNotFoundException {

        String mail = "test@test.com";
        UserModel user = new UserModel();
        user.setMail(mail);
        user.setId(1);
        user.setWallet(BigDecimal.valueOf(12));
        user.setBanques(new HashSet<>());

        BanqueDto banquedto = new BanqueDto();
        banquedto.setId(1);
        banquedto.setBic("Error");
        banquedto.setIban("Iban");
        banquedto.setNom("nom");

        TransferDto transfer = new TransferDto();
        transfer.setBank(banquedto);
        transfer.setAmount(BigDecimal.valueOf(12));
        when(repository.findByMail(mail)).thenReturn(Optional.of(user));

        AccountNotFoundException thrown = assertThrows(AccountNotFoundException.class,
                () -> service.deposit(mail, transfer));

        assertThat(thrown.getMessage()).isEqualToIgnoringCase("No bank account founded with specified informations");
    }

    @Test
    public void withdraw_shouldremoveMoneytoWallet() throws AccountNotFoundException, NoMoneyException {

        String mail = "test@test.com";
        UserModel user = new UserModel();
        user.setMail(mail);
        user.setId(1);
        user.setWallet(BigDecimal.valueOf(15));

        Banque banque = new Banque();
        banque.setId(1);
        banque.setBic("Bic");
        banque.setIban("Iban");
        banque.setNom("nom");

        Set<Banque> banques = new HashSet<>();
        banques.add(banque);

        user.setBanques(banques);

        BanqueDto banquedto = new BanqueDto();
        banquedto.setId(1);
        banquedto.setBic("Bic");
        banquedto.setIban("Iban");
        banquedto.setNom("nom");

        TransferDto transfer = new TransferDto();
        transfer.setBank(banquedto);
        transfer.setAmount(BigDecimal.valueOf(10));

        when(repository.findByMail(mail)).thenReturn(Optional.of(user));
        when(repository.save(any())).thenReturn(new UserModel());

        ArgumentCaptor<UserModel> argument = ArgumentCaptor.forClass(UserModel.class);

        service.withdraw(mail, transfer);

        verify(repository).save(argument.capture());
        assertThat(argument.getValue().getWallet()).isEqualTo(BigDecimal.valueOf(5));

    }

    @Test
    public void withdraw_ShouldThrowExceptiont() throws AccountNotFoundException {

        String mail = "test@test.com";
        TransferDto transfer = new TransferDto();

        when(repository.findByMail(mail)).thenReturn(Optional.empty());

        AccountNotFoundException thrown = assertThrows(AccountNotFoundException.class,
                () -> service.withdraw(mail, transfer));

        assertThat(thrown.getMessage()).isEqualToIgnoringCase(accountMessage);
    }

    @Test
    public void withdraw_shouldThrowErrorwhenBankaccountisinvalid() throws AccountNotFoundException {

        String mail = "test@test.com";
        UserModel user = new UserModel();
        user.setMail(mail);
        user.setId(1);
        user.setWallet(BigDecimal.valueOf(12));

        Banque banque = new Banque();
        banque.setId(1);
        banque.setBic("Bic");
        banque.setIban("Iban");
        banque.setNom("nom");

        Set<Banque> banques = new HashSet<>();
        banques.add(banque);

        user.setBanques(banques);

        BanqueDto banquedto = new BanqueDto();
        banquedto.setId(1);
        banquedto.setBic("Error");
        banquedto.setIban("Iban");
        banquedto.setNom("nom");

        TransferDto transfer = new TransferDto();
        transfer.setBank(banquedto);
        transfer.setAmount(BigDecimal.valueOf(12));
        when(repository.findByMail(mail)).thenReturn(Optional.of(user));

        AccountNotFoundException thrown = assertThrows(AccountNotFoundException.class,
                () -> service.withdraw(mail, transfer));

        assertThat(thrown.getMessage()).isEqualToIgnoringCase("No bank account founded with specified informations");
    }

    @Test
    public void withdraw_shouldThrowErrorwhenBankaccountisinvalid2() throws AccountNotFoundException {

        String mail = "test@test.com";
        UserModel user = new UserModel();
        user.setMail(mail);
        user.setId(1);
        user.setWallet(BigDecimal.valueOf(12));

        Banque banque = new Banque();
        banque.setId(1);
        banque.setBic("Bic");
        banque.setIban("Iban");
        banque.setNom("nom");

        Set<Banque> banques = new HashSet<>();
        banques.add(banque);

        user.setBanques(banques);

        BanqueDto banquedto = new BanqueDto();
        banquedto.setId(1);
        banquedto.setBic("Bic");
        banquedto.setIban("Error");
        banquedto.setNom("nom");

        TransferDto transfer = new TransferDto();
        transfer.setBank(banquedto);
        transfer.setAmount(BigDecimal.valueOf(12));
        when(repository.findByMail(mail)).thenReturn(Optional.of(user));

        AccountNotFoundException thrown = assertThrows(AccountNotFoundException.class,
                () -> service.withdraw(mail, transfer));

        assertThat(thrown.getMessage()).isEqualToIgnoringCase("No bank account founded with specified informations");
    }

    @Test
    public void withdraw_shouldThrowErrorwhenBankaccountisinvalid3() throws AccountNotFoundException {

        String mail = "test@test.com";
        UserModel user = new UserModel();
        user.setMail(mail);
        user.setId(1);
        user.setWallet(BigDecimal.valueOf(12));
        user.setBanques(new HashSet<>());

        BanqueDto banquedto = new BanqueDto();
        banquedto.setId(1);
        banquedto.setBic("Bic");
        banquedto.setIban("Iban");
        banquedto.setNom("nom");

        TransferDto transfer = new TransferDto();
        transfer.setBank(banquedto);
        transfer.setAmount(BigDecimal.valueOf(12));
        when(repository.findByMail(mail)).thenReturn(Optional.of(user));

        AccountNotFoundException thrown = assertThrows(AccountNotFoundException.class,
                () -> service.deposit(mail, transfer));

        assertThat(thrown.getMessage()).isEqualToIgnoringCase("No bank account founded with specified informations");
    }

    @Test
    public void withdraw_shouldThrowNoMoneyException() throws AccountNotFoundException, NoMoneyException {

        String mail = "test@test.com";
        UserModel user = new UserModel();
        user.setMail(mail);
        user.setId(1);
        user.setWallet(BigDecimal.valueOf(5));

        Banque banque = new Banque();
        banque.setId(1);
        banque.setBic("Bic");
        banque.setIban("Iban");
        banque.setNom("nom");

        Set<Banque> banques = new HashSet<>();
        banques.add(banque);

        user.setBanques(banques);

        BanqueDto banquedto = new BanqueDto();
        banquedto.setId(1);
        banquedto.setBic("Bic");
        banquedto.setIban("Iban");
        banquedto.setNom("nom");

        TransferDto transfer = new TransferDto();
        transfer.setBank(banquedto);
        transfer.setAmount(BigDecimal.valueOf(10));

        when(repository.findByMail(mail)).thenReturn(Optional.of(user));

        NoMoneyException thrown = assertThrows(NoMoneyException.class,
                () -> service.withdraw(mail, transfer));

        assertThat(thrown.getMessage())
                .isEqualToIgnoringCase("You don't have enough money on your account to perform this action");

    }
}