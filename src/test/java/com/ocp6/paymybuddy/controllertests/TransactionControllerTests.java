package com.ocp6.paymybuddy.controllertests;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

import com.ocp6.paymybuddy.config.exceptions.AccountNotFoundException;
import com.ocp6.paymybuddy.config.exceptions.NoMoneyException;
import com.ocp6.paymybuddy.config.jwt.JwtUtils;
import com.ocp6.paymybuddy.controller.TransactionController;
import com.ocp6.paymybuddy.dto.TransactionDto;
import com.ocp6.paymybuddy.services.TransactionService;

@ExtendWith(MockitoExtension.class)
public class TransactionControllerTests {

    @Mock
    JwtUtils utils;

    @Mock
    TransactionService service;

    @InjectMocks
    TransactionController controller;

    @Test
    public void transferMoney() throws AccountNotFoundException, NoMoneyException {

        TransactionDto dto = new TransactionDto();
        when(utils.getTokenFromHeader(anyString())).thenReturn("TOKEN");
        when(utils.getUsernamefromToken(anyString())).thenReturn("mail");

        controller.transferMoney("Bearer TOKEN", dto);

        verify(utils).getTokenFromHeader("Bearer TOKEN");
        verify(utils).getUsernamefromToken("TOKEN");
        verify(service).transferMoney("mail", dto);

    }

    @Test
    public void getCommisson() {

        ResponseEntity<BigDecimal> result = controller.getCommisson();

        assertThat(result).isEqualToComparingFieldByField(
                ResponseEntity.status(200).body(TransactionService.COMMISSION_PERCENTAGE));

    }

}
