package com.ocp6.paymybuddy.controllertests;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.ocp6.paymybuddy.config.exceptions.DatabaseErrorException;
import com.ocp6.paymybuddy.config.jwt.JwtUtils;
import com.ocp6.paymybuddy.controller.AuthController;
import com.ocp6.paymybuddy.dto.AuthRequest;
import com.ocp6.paymybuddy.dto.Subscriber;
import com.ocp6.paymybuddy.dto.TokenVerify;
import com.ocp6.paymybuddy.services.AuthenticationService;

@ExtendWith(MockitoExtension.class)
public class AuthControllerTests {

    @Mock
    private AuthenticationService service;

    @Mock
    AuthenticationManager authenticationManager;

    @Mock
    PasswordEncoder encoder;

    @Mock
    private JwtUtils utils;

    @InjectMocks
    AuthController controller;

    @Test
    public void login_shouldauthenticate() {

        AuthRequest request = new AuthRequest();
        UserDetails user = new User("user", "password",
                Collections.singleton(new SimpleGrantedAuthority("ROLE_USER")));

        when(service.loadUserByUsername(any())).thenReturn(user);
        when(authenticationManager.authenticate(any()))
                .thenReturn(new UsernamePasswordAuthenticationToken("user", "password"));
        when(utils.generateToken(any())).thenReturn("Token");

        ResponseEntity<String> response = controller.login(request);

        verify(service).loadUserByUsername(any());
        verify(authenticationManager).authenticate(any());
        verify(utils).generateToken(any());

        ResponseEntity<String> expected = ResponseEntity.status(200).body("Token");
        assertThat(response.getStatusCode()).isEqualTo(expected.getStatusCode());
        assertThat(response.getBody()).isEqualTo(expected.getBody());

    }

    @Test
    public void login_shouldreturnError_WhenusernamenotFound() {

        AuthRequest request = new AuthRequest();

        when(service.loadUserByUsername(any())).thenThrow(new UsernameNotFoundException(null, null));

        ResponseEntity<String> response = controller.login(request);

        ResponseEntity<String> expected = ResponseEntity.status(401).body("No user found with this e-mail");
        assertThat(response.getStatusCode()).isEqualTo(expected.getStatusCode());
        assertThat(response.getBody()).isEqualTo(expected.getBody());
    }

    @Test
    public void login_shouldreturnError_Whenpasswordnotfound() {

        AuthRequest request = new AuthRequest();

        when(service.loadUserByUsername(any())).thenThrow(new BadCredentialsException(""));

        ResponseEntity<String> response = controller.login(request);

        ResponseEntity<String> expected = ResponseEntity.status(401).body("Incorrect Password");
        assertThat(response.getStatusCode()).isEqualTo(expected.getStatusCode());
        assertThat(response.getBody()).isEqualTo(expected.getBody());
    }

    @Test
    public void verifyMail() {
        controller.verifyMail("mail");
        verify(service).verifyMail("mail");
    }

    @Test
    public void subscribe() throws DatabaseErrorException {
        Subscriber user = new Subscriber();
        ResponseEntity<String> response = controller.subscribe(user);

        verify(service).subscribe(user, encoder);

        ResponseEntity<String> expected = new ResponseEntity<>("Account successfully created", HttpStatus.CREATED);

        assertThat(response).isEqualToComparingFieldByField(expected);
    }

    @Test
    public void verifyToken() {
        TokenVerify token = new TokenVerify();
        token.setMail("mail");
        token.setToken("TOKEN");

        controller.verifyToken(token);

        verify(utils).validateToken(anyString(), any());

    }
}
