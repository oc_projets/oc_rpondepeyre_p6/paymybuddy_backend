package com.ocp6.paymybuddy.controllertests;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

import com.ocp6.paymybuddy.config.exceptions.AccountNotFoundException;
import com.ocp6.paymybuddy.config.exceptions.BadRequestException;
import com.ocp6.paymybuddy.config.jwt.JwtUtils;
import com.ocp6.paymybuddy.controller.UserController;
import com.ocp6.paymybuddy.dto.BanqueDto;
import com.ocp6.paymybuddy.dto.CurrentUserDto;
import com.ocp6.paymybuddy.dto.TransferDto;
import com.ocp6.paymybuddy.dto.UserDto;
import com.ocp6.paymybuddy.services.UserService;

@ExtendWith(MockitoExtension.class)
public class UserControllerTests {

    @Mock
    JwtUtils utils;

    @Mock
    UserService service;

    @InjectMocks
    UserController controller;

    @Test
    public void getCurrentUserInfos() throws AccountNotFoundException {

        CurrentUserDto user = new CurrentUserDto();
        when(utils.getTokenFromHeader(anyString())).thenReturn("TOKEN");
        when(utils.getUsernamefromToken(anyString())).thenReturn("mail");
        when(service.findCurrentUserByMail("mail")).thenReturn(user);

        ResponseEntity<CurrentUserDto> response = controller.getCurrentUserInfos("Bearer TOKEN");

        verify(service).findCurrentUserByMail("mail");
        assertThat(response).isEqualToComparingFieldByField(ResponseEntity.status(200).body(user));

    }

    @Test
    public void getUserByMail() throws AccountNotFoundException {

        UserDto user = new UserDto();
        when(service.findUserByMail("mail")).thenReturn(user);
        ResponseEntity<UserDto> response = controller.getUserByMail("mail");
        verify(service).findUserByMail("mail");

        assertThat(response).isEqualToComparingFieldByField(ResponseEntity.status(200).body(user));
    }

    @Test
    public void addConnection() throws AccountNotFoundException, BadRequestException {

        CurrentUserDto user = new CurrentUserDto();
        when(utils.getTokenFromHeader(anyString())).thenReturn("TOKEN");
        when(utils.getUsernamefromToken(anyString())).thenReturn("mail");
        when(service.addConnection("mail", "addmail")).thenReturn(user);

        ResponseEntity<CurrentUserDto> response = controller.addConnection("mail", "addmail");

        verify(service).addConnection("mail", "addmail");
        assertThat(response).isEqualToComparingFieldByField(ResponseEntity.status(200).body(user));
    }

    @Test
    public void removeConnection() throws AccountNotFoundException, BadRequestException {

        CurrentUserDto user = new CurrentUserDto();
        when(utils.getTokenFromHeader(anyString())).thenReturn("TOKEN");
        when(utils.getUsernamefromToken(anyString())).thenReturn("mail");
        when(service.removeConnection("mail", "removemail")).thenReturn(user);

        ResponseEntity<CurrentUserDto> response = controller.removeConnection("mail", "removemail");

        verify(service).removeConnection("mail", "removemail");
        assertThat(response).isEqualToComparingFieldByField(ResponseEntity.status(200).body(user));
    }

    @Test
    public void addBankAccount() throws AccountNotFoundException, BadRequestException {

        CurrentUserDto user = new CurrentUserDto();
        BanqueDto dto = new BanqueDto();

        when(utils.getTokenFromHeader(anyString())).thenReturn("TOKEN");
        when(utils.getUsernamefromToken(anyString())).thenReturn("mail");
        when(service.addBankAccount("mail", dto)).thenReturn(user);

        ResponseEntity<CurrentUserDto> response = controller.addBankAccount("mail", dto);

        verify(service).addBankAccount("mail", dto);
        assertThat(response).isEqualToComparingFieldByField(ResponseEntity.status(200).body(user));
    }

    @Test
    public void removeBankAccount() throws AccountNotFoundException, BadRequestException {

        CurrentUserDto user = new CurrentUserDto();

        when(utils.getTokenFromHeader(anyString())).thenReturn("TOKEN");
        when(utils.getUsernamefromToken(anyString())).thenReturn("mail");
        when(service.removeBankAccount("mail", 1)).thenReturn(user);

        ResponseEntity<CurrentUserDto> response = controller.removeBankAccount("mail", 1);

        verify(service).removeBankAccount("mail", 1);
        assertThat(response).isEqualToComparingFieldByField(ResponseEntity.status(200).body(user));
    }

    @Test
    public void deposit() throws AccountNotFoundException, BadRequestException {

        CurrentUserDto user = new CurrentUserDto();
        TransferDto dto = new TransferDto();

        when(utils.getTokenFromHeader(anyString())).thenReturn("TOKEN");
        when(utils.getUsernamefromToken(anyString())).thenReturn("mail");
        when(service.deposit("mail", dto)).thenReturn(user);

        ResponseEntity<CurrentUserDto> response = controller.deposit("mail", dto);

        verify(service).deposit("mail", dto);
        assertThat(response).isEqualToComparingFieldByField(ResponseEntity.status(200).body(user));
    }

    @Test
    public void withdraw() throws Exception {

        CurrentUserDto user = new CurrentUserDto();
        TransferDto dto = new TransferDto();

        when(utils.getTokenFromHeader(anyString())).thenReturn("TOKEN");
        when(utils.getUsernamefromToken(anyString())).thenReturn("mail");
        when(service.withdraw("mail", dto)).thenReturn(user);

        ResponseEntity<CurrentUserDto> response = controller.withdraw("mail", dto);

        verify(service).withdraw("mail", dto);
        assertThat(response).isEqualToComparingFieldByField(ResponseEntity.status(200).body(user));
    }

}
