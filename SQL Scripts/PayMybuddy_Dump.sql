CREATE SCHEMA paymybuddy;
ALTER SCHEMA paymybuddy OWNER TO postgres;
CREATE TABLE paymybuddy.banque (
    id integer NOT NULL,
    nom character varying NOT NULL,
    utilisateur_id integer NOT NULL,
    iban character varying NOT NULL,
    bic character varying NOT NULL
);
ALTER TABLE paymybuddy.banque OWNER TO postgres;
CREATE SEQUENCE paymybuddy.banque_id_seq START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE CACHE 1;
ALTER TABLE paymybuddy.banque_id_seq OWNER TO postgres;
ALTER SEQUENCE paymybuddy.banque_id_seq OWNED BY paymybuddy.banque.id;
CREATE TABLE paymybuddy.relation (
    ajouteur_id integer NOT NULL,
    ajouted_id integer NOT NULL
);
ALTER TABLE paymybuddy.relation OWNER TO postgres;
CREATE TABLE paymybuddy.transaction (
    id integer NOT NULL,
    description character varying,
    montant numeric NOT NULL,
    date timestamp without time zone NOT NULL,
    envoyeur_id integer NOT NULL,
    receveur_id integer NOT NULL
);
ALTER TABLE paymybuddy.transaction OWNER TO postgres;
CREATE SEQUENCE paymybuddy.transaction_id_seq START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE CACHE 1;
ALTER TABLE paymybuddy.transaction_id_seq OWNER TO postgres;
ALTER SEQUENCE paymybuddy.transaction_id_seq OWNED BY paymybuddy.transaction.id;
CREATE TABLE paymybuddy.utilisateur (
    id integer NOT NULL,
    mail character varying NOT NULL,
    password character varying NOT NULL,
    nom character varying NOT NULL,
    prenom character varying NOT NULL,
    wallet numeric DEFAULT 0 NOT NULL
);
ALTER TABLE paymybuddy.utilisateur OWNER TO postgres;
CREATE SEQUENCE paymybuddy.utilisateur_id_seq AS integer START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE CACHE 1;
ALTER TABLE paymybuddy.utilisateur_id_seq OWNER TO postgres;
ALTER SEQUENCE paymybuddy.utilisateur_id_seq OWNED BY paymybuddy.utilisateur.id;
ALTER TABLE ONLY paymybuddy.banque
ALTER COLUMN id
SET DEFAULT nextval('paymybuddy.banque_id_seq'::regclass);
ALTER TABLE ONLY paymybuddy.transaction
ALTER COLUMN id
SET DEFAULT nextval('paymybuddy.transaction_id_seq'::regclass);
ALTER TABLE ONLY paymybuddy.utilisateur
ALTER COLUMN id
SET DEFAULT nextval('paymybuddy.utilisateur_id_seq'::regclass);
ALTER TABLE ONLY paymybuddy.banque
ADD CONSTRAINT banque_pk PRIMARY KEY (id);
ALTER TABLE ONLY paymybuddy.transaction
ADD CONSTRAINT transaction_pk PRIMARY KEY (id);
ALTER TABLE ONLY paymybuddy.utilisateur
ADD CONSTRAINT utilisateur_pk PRIMARY KEY (id);
ALTER TABLE ONLY paymybuddy.banque
ADD CONSTRAINT banque_fk FOREIGN KEY (utilisateur_id) REFERENCES paymybuddy.utilisateur(id);
ALTER TABLE ONLY paymybuddy.relation
ADD CONSTRAINT relation_fk FOREIGN KEY (ajouteur_id) REFERENCES paymybuddy.utilisateur(id);
ALTER TABLE ONLY paymybuddy.relation
ADD CONSTRAINT relation_fk_1 FOREIGN KEY (ajouted_id) REFERENCES paymybuddy.utilisateur(id);
ALTER TABLE ONLY paymybuddy.transaction
ADD CONSTRAINT transaction_fk FOREIGN KEY (envoyeur_id) REFERENCES paymybuddy.utilisateur(id);
ALTER TABLE ONLY paymybuddy.transaction
ADD CONSTRAINT transaction_fk_1 FOREIGN KEY (receveur_id) REFERENCES paymybuddy.utilisateur(id);